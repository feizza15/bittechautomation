package org.gs.ui.utility;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;

public class Support
{
	public WebDriver dr;
	public Support(WebDriver dr)
	{
			this.dr=dr;
	}
	public void click(By by)
	{
		dr.findElement(by).click();
		
	}
	
	public void elementSize(By ele)
	{
		System.out.println(dr.findElements(ele).size());
	}
}
